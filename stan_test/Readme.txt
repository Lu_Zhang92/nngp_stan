# Introduction:

This is a test folder for implementing Full Gaussian process and Nearest 
Neighborhood Gaussian Process on spatial data in Stan. For more details 
about synthetic data, models, and simulation results, please refer to 
“summary.pdf”.  


# How to run the program:

The language interface to perform the analysis is Rstan. All the executable files 
are in subfolder “projects” and Stan codes are saved in subfolder “src”. To run 
the analysis, open R program in “projects” and set the working directory to be 
the path of this folder. R code in subfolder “projects” will load data and link 
Stan codes with a relative path. 


# Folder Structure:

data: 		Synthetic data and parameters for priors

projects: 	Rstan codes for two models.

results: 	Simulation results

src: 		Stan codes

 








