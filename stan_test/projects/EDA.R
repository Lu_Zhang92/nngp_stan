# Exploration of Data Analysis #
setwd("/stan_test")
rm(list = ls())
load("./data/nngp_w_15.RData")

# Check Nearest neigborhood #
par(mfrow = c(1, 1))
for(i in 700:710){
  plot(coords) # all points
  points(coords[1:i, , drop = FALSE], col = "grey", pch = 19) # points 1:i
  points(coords[i, , drop = FALSE], col = "blue", pch = 19) # the target point
  
  # neighborhood
  if (i < M) {dim = i} else {dim = M}
  for (j in 1:dim){
    points(coords[nearind[i - 1, j], , drop = FALSE], col = "orange", 
           pch = 19)
    points(coords[nearind[i - 1, j], , drop = FALSE], col = "orange", 
           pch = as.character(j), cex = 2)
  }
  readline(prompt = "Pause. Press <Enter> to continue...")
}


# Check Variogram plot #
# get estimate of sill range and nugget #
library(spBayes)
library(geoR)
max.dist <- 0.5 * max(dist(coords))
#usually transfer coordinate into two dimension instead of using longtitude and latitude
bins <- 50

lm.Y <- lm(Y ~ X[, 2])
summary(lm.Y)
Y.resid <- resid(lm.Y)

vario.Y.resid <- variog(coords = coords, data = Y.resid, uvec = 
                          (seq(0, max.dist, length = bins)))
fit.Y.resid <- variofit(vario.Y.resid, ini.cov.pars = 
                          c(1.6, 0.5 * max.dist / (- log(0.05))), 
                        cov.model = "exponential", minimisation.function = 
                          "nls", weights = "equal")

par(mfrow = c(1, 1))
#look at the residual to find possible effective predictors
plot(vario.Y.resid, ylim = range(0, 2), main = "Y residuals")
lines(fit.Y.resid)
abline(h = fit.Y.resid$nugget, col = "blue")
abline(h = fit.Y.resid$cov.pars[1] + fit.Y.resid$nugget, col = "green")
abline(v = - log(0.05) * fit.Y.resid$cov.pars[2], col = "red3")
summary(fit.Y.resid)$estimated.pars

# The estimate of tausq 0.1121
# The estimate of sigmasq 1.5245862
# The estimate of spatial decay 7.924

