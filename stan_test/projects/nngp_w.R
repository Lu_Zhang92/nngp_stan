# set the working directory below:
setwd(" ")
rm(list = ls())
load("./data/nngp_w_15.RData")

### input data ###
#N:       int, number of locations = 1000
#M:       int, number of neighborhood = 15
#P:       int, number of covariates (intercept and one covariate) = 2
#Y:       double, the response
#X:       double matrix (N by P), explanatory covariates, first colume is 1.     
#nearind: int matrix (N-1 by M), records the index of nearest neighborhood
#         the ith row (i_1, i_2, ..., i_M) indicates that (i_1th, i_2th, ...,
#         i_pth) location consist the neighhorhood of ith location.
#neardist: double matrix (N-1 by M), records distances from nearest neighborhood
#         the ith row (d_i1, d_i2, ..., d_iM) saves the distrance between ith 
#         location and (i_1th, i_2th, ..., i_Mth) location respectively.
#neardistM: double matrix (N-1 by M*M), records distance matrix among 
#         the nearest neighborhood. 
#w:       double array, the true value of spatial random effect.

### Parameters for Priors ###
# Beta ~ flat
# sigmasq ~ inv_gamma(as, bs)
#as:      double, = 2.0
#bs:      double, = 1.5
# tausq ~ inv_gamma(at, bt)
#at:      double, = 2.0
#bt:      double, = 0.1
# phi ~ uniform(ap, bp)
#ap:      int, = 4.0
#bp:      int, = 30.0
# w approximate MVN(0, sigmasq * exp(- phi * dist)) by NNGP


library(rstan)
data <- list(N = N, M = M, P = P, Y = Y, X = X, nearind = nearind, 
             neardist = neardist, neardistM = neardistM, 
             as = as, bs = bs, at = at, bt = at, ap = ap, bp = bp)

myinits <-list(
  list(beta = c(1, -50), sigmasq = 6, tausq = 1, phi = 10,
       w = w_fit))  # chain 1 starting value


parameters <- c("beta", "sigmasq", "tausq", "phi", "w")

samples_w <- stan(
  file = "./src/nngp_w.stan",
  data = data,
  init = myinits,
  pars = parameters,
  iter = 2000, 
  chains = 1, 
  thin = 1
)

# save the print output in an extent file
sink("./results/samples_w_print2.txt")
print(samples_w)
sink()

stan_trace(samples_w)
save(samples_w, file = "./results/model2_nngp_15")
load("./results/model2_nngp_18")

# check the results with package rstan
stan_plot(samples_w)
stan_trace(samples_w)
stan_hist(samples_w)
stan_ac(samples_w)

# check the raw output
library(coda)
library(lattice)
library(corrplot)
sim_w <- As.mcmc.list(samples_w)
sim_w_post <- sim_w[[1]]

# check how many 95% CI for w cover the true value.
myqnt <- c(0.5, 0.025, 0.975)
col.qnt <- function(X, start, niter, myqnt){
  return (c(quantile(X[start:niter], myqnt)))
}
w_stan.qnt <-apply(sim_w_post[, 5:1004], 2, col.qnt, 1, 1000, myqnt)
count = 0
for (i in 1:n){
  count = count + ((w[i] > w_stan.qnt[2, i]) & (w[i] < w_stan.qnt[3, i]))
};count
