/* NNGP with w */

functions{
real nngp_w_lpdf(vector w, real sigmasq, real phi, matrix neardist, matrix neardistM,
                int[,] nearind, int N, int M){

        vector[N] V;
        vector[N] Uw;
        int dim;
        int int_one;
        real out;
        Uw = w;
        for (i in 2:N) {

            matrix[ i < (M + 1)? (i - 1) : M, i < (M + 1)? (i - 1): M] temp_neardistM;
            matrix[ i < (M + 1)? (i - 1) : M, i < (M + 1)? (i - 1): M] L;
            vector[ i < (M + 1)? (i - 1) : M] u;
            vector[ i < (M + 1)? (i - 1) : M] v;
            row_vector[i < (M + 1)? (i - 1) : M] v2;

            dim = (i < (M+1))? (i-1) : M;

            // get exp(-phi * neardistM)
            for (j in 1:dim){
                for (k in 1:dim){
                    temp_neardistM[j, k] = exp(- phi * neardistM[(i - 1), (j - 1) * dim + k]);
                }
            }

            L = cholesky_decompose(temp_neardistM);

            for (j in 1: dim){
                u[j] = exp(- phi * neardist[(i - 1), j]);
            }
            
            //vector[dim] v;
            v = mdivide_left_tri_low(L, u);

            V[i] = 1 - (v' * v);

            v2 = mdivide_right_tri_low(v', L);

            for (j in 1:dim){
                Uw[i] = Uw[i] - v2[j] * w[nearind[(i-1), j]];
            }
        }
        V[1] = 1;
        out = 0.0;
        for (i in 1:N){
            out = out - 0.5 * log(V[i]) - 0.5 / sigmasq * (Uw[i] * Uw[i] / V[i]);
        }
        out = out - 0.5 * N * log(sigmasq);
        return out;
    }
}

data {
    int N;
    int M;
    int P;
    vector[N] Y;
    matrix[N, P] X;
    int nearind[N-1, M];
    matrix[N-1, M] neardist;
    matrix[N-1, M*M] neardistM;
    real as;
    real bs;
    real at;
    real bt;
    real ap;
    real bp;
}

parameters{
    vector[2] beta;
    real<lower = 0> sigmasq;
    real<lower = 0> tausq;
    real<lower = ap, upper = bp> phi;
    vector[N] w;
}

model{
    sigmasq ~ inv_gamma(as, bs);
    tausq ~ inv_gamma(at, bt);
    w ~ nngp_w(sigmasq, phi, neardist, neardistM, nearind, N, M);
    for (i in 1:N){
        Y[i] ~ normal((X[i,] * beta + w[i]), tausq);
    }
}
















