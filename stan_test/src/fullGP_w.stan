/* Full Gaussian Process with w */

functions{
matrix get_w_L(real sigmasq, real tausq, real phi, matrix dist, int N){

    matrix[N, N] temp_dist;
    matrix[N, N] L;

    for (j in 1:N){
        for (k in 1:N){
            temp_dist[j, k] = sigmasq * exp(- phi * dist[j, k]);
        }
    }
    L = cholesky_decompose(temp_dist);
    return L;
    }
}

data {
    int<lower=1> N;
    int<lower=1> P;
    vector[N] Y;
    matrix[N, P] X;
    matrix[N, N] dist;
    real as;
    real bs;
    real at;
    real bt;
    int ap;
    int bp;
    vector[N] mu_w;
}

parameters {
    vector[2] beta;
    real<lower = 0> sigmasq;
    real<lower = 0> tausq;
    real<lower = ap, upper = bp> phi;
    vector[N] w;
}


model{
    sigmasq ~ inv_gamma(as, bs);
    tausq ~ inv_gamma(at, bt);
    w ~ multi_normal_cholesky(mu_w, get_w_L(sigmasq, tausq, phi, dist, N));
    for (i in 1:N){
        Y[i] ~ normal((X[i,] * beta + w[i]), tausq);
    }
}











